require auth

## Access security environment variables
epicsEnvSet("IOCNAME", "test-ioc")
epicsEnvSet("PATH_TO_ASG_FILES", "$(auth_DIR)")
epicsEnvSet("ASG_FILENAME", "$(ASG_FILENAME=unrestricted_access.acf)")

# set up access security
# disable for now due to failing test
iocshLoad("$(auth_DIR)/accessSecurityGroup.iocsh", "ASG_PATH=$(PATH_TO_ASG_FILES),ASG_FILE=$(ASG_FILENAME)")
